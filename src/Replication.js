import React from 'react';
import Toggle from './Toggle';
import CheckBox from "@material-ui/core/es/internal/svg-icons/CheckBox";
import Typography from "@material-ui/core/Typography/Typography";
import Switch from "@material-ui/core/Switch/Switch";
import Grid from "@material-ui/core/Grid/Grid";

const dcs = {us1: false, eu1: false, au1: false, ru1: false, cn1: false};

class Replication extends React.Component {
    constructor(props) {
        super(props);
        this.state = {replication: props.replication || dcs};
        this.handleChange = this.handleChange.bind(this);

    }

    handleChange(event, dc) {
        //  this.state.replication[dc] = event.target.checked
        const replication = this.state.replication;
        replication[dc] = event.target.checked;
        this.setState({replication: replication});
        this.props.onChange(replication);
        // this.setState({replication: {[dc] :event.target.checked }})
    }

    render() {
        return (
            <React.Fragment >
                <Grid  container direction={"column"}  justify={"space-between"} alignItems={"stretch"}>
                {Object.entries(this.state.replication)
                    .map(([dc, checked]) => (
                        <Grid key={dc}  container direction={"row"}  justify={"space-between"} alignItems={"stretch"}>
                            <Switch color="primary" type="checkbox" checked={checked} title={dc} value={dc}
                                                onChange={(e) => this.handleChange(e, dc)} />

                            <Typography gutterBottom variant="h6" component="h6">
                                {dc}
                            </Typography>

                        </Grid>))}
                </Grid>
            </React.Fragment>);
    }
}

export default Replication