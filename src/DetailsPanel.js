import {createGlobalState} from "react-context-global-state";
import React from "react";
import Paper from "@material-ui/core/Paper/Paper";

const initialState = {
     details: <div />
};
const { StateProvider, StateConsumer } = createGlobalState(initialState);


const DetailsPanel = () => (

    <div className={"details"}>
        <StateProvider >

    <StateConsumer name="details">
         {(value, update) => (

            <React.Fragment>
                {value}
            </React.Fragment>)

            }
    </StateConsumer>
        </StateProvider>
    </div>
);

const DetailsPanelSetter = (props ) => (
    <StateProvider >

        <StateConsumer name="details">
        {(value, update) => (
            <div  className={value === props.details? "div-selected" : "div"} onClick={()=> update(props.details)}><React.Fragment> {props.set}   </React.Fragment></div>
         )}
    </StateConsumer>
    </StateProvider>
);



export  {  DetailsPanelSetter , DetailsPanel}



