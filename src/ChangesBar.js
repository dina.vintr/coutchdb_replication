import React, {Component} from 'react';
import ChangeMessage from './ChangeMessage';



class ChangesBar extends Component {
    constructor(props) {
        super(props);
        this.state = {changes: [], deleted: [], onChange: props.onChange, enabled: props.enabled, lastChanges: []};
    }

    componentWillMount() {
        this.enabledChanged();
    }

    enabledChanged() {
        if (this.state.enabled) {
            this.fetchChanges( );
        }
        else if (this.state.subscriber) {
            this.state.subscriber.cancel();
        }
    }

    toggle(toggle){
        this.setState({enabled: toggle});
    }

    componentDidUpdate(prevProps, prevState, ss) {
        if (this.state.enabled != prevState.enabled)
            this.enabledChanged();

    }

    fetchChanges( ) {
        const db = this.props.db;

        const subscriber = db.changes({
            since: 'now',
            include_docs: true,
            conflicts: true,
            live: true,
            heartbeat: 50000,
            timeout: 50000
        })
            .on('change', change => {

                if (this.state.onChange)
                    this.state.onChange(change)
                const changes = this.state.changes;
                changes.push(change);
                const lastChanges= [];
                lastChanges.push(change);
                this.setState({changes: changes , lastChanges:lastChanges});

            })
            .on('error', err => console.log(err));

        this.state.subscriber = subscriber;
    }


    componentWillUnmount() {
        if( this.state.subscriber)
        this.state.subscriber.cancel();
    }




    render() {
        let changes = this.state.lastChanges;
const div=
    <div>
        {changes.map(ch =>  <ChangeMessage key={ch.seq} change={ch} dc={this.props.dc} />)}

    </div>;

        this.state.lastChanges= [];

        return div;}



}


export default   ChangesBar;




