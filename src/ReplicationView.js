import React, {Component} from 'react';
import './App.css';
import {Rectangle, Circle, Ellipse, Line, Polyline, CornerBox, Triangle} from 'react-shapes';
import CShape from './CShape'
//import Table1 from './Table1'
// import {PouchDB, Find, Get, withDB, _withDB} from 'react-pouchdb' ;
import PouchDB from 'pouchdb';
import {ButtonToolbar} from "react-bootstrap";
// import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import PopupState, {bindTrigger, bindMenu, bindToggle, bindPopper, bindPopover} from 'material-ui-popup-state/index';
import Replication from "./Replication";
import DocsView from "./DocsView";
import Switch from "@material-ui/core/Switch/Switch";
import ChangesBar from "./ChangesBar";
import Chip from "@material-ui/core/Chip/Chip";
import Icon from "@material-ui/core/Icon/Icon";
import Popper from "@material-ui/core/Popper/Popper";
import Fade from "@material-ui/core/Fade/Fade";
import Paper from "@material-ui/core/Paper/Paper";
import HighlightJs from "./HighlightJs";
import Popover from "@material-ui/core/Popover/Popover";
import {createGlobalState} from "react-context-global-state";
import {useGlobal} from "reactn";
import CounterContainer, {DetailsPanel, DetailsPanelSetter} from "./DetailsPanel";
import CssBaseline from "@material-ui/core/CssBaseline/CssBaseline";
import {SnackbarProvider} from "notistack";

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    appBar: {
        position: 'relative',
    },
    icon: {
        marginRight: theme.spacing.unit * 2,
    },
    heroUnit: {
        backgroundColor: theme.palette.background.paper,
    },
    heroContent: {
        maxWidth: 600,
        margin: '0 auto',
        padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
    },
    heroButtons: {
        marginTop: theme.spacing.unit * 4,
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
            width: 1100,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    cardGrid: {
        padding: `${theme.spacing.unit * 8}px 0`,
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing.unit * 6,
    },
});



class ReplicationView extends Component {

    state={docs:[]};
    componentWillMount() {
        const db = new PouchDB(`http://10.200.0.166:5984/_replicator`);
         db.allDocs({include_docs: true  }).then(res =>
            this.setState({docs: res.rows.map(row => row.doc)}));


    }



    render() {

        let docs = this.state.docs;
         return (
             <div>
                   <Grid container direction="row"
                      justify="space-evenly"
                      alignItems="center"
                      spacing={24} label={"anchorPosition"}>
                    {docs.map(ch => this.renderDoc(ch))}
                </Grid>

             </div>
        );
    }


    renderDoc(doc) {

         if (doc._id === "_design/_replicator" || doc._id === "2test1"  ) {
            return <div key={doc._id} hidden={true}></div>;
        }

        return (

                   <div>

                       <DetailsPanelSetter

                           details={this.renderDetails(doc)}
                           set={
                           <Chip
                               className={`chip default`}
                               variant={"outlined"}
                               width={900}
                               label={<div height={500}>
                                   <b   >
                                       <pre>id: {doc._id} </pre>
                                       <pre>from: {doc.source.url}</pre>
                                       <pre>to: {doc.target.url} </pre>

                                   </b></div>}

                           >


                           </Chip>
                       }/>



                    </div>
                 );

    }


    renderDetails(change) {
        return (
            <Card>
                <b>replication config</b>
                <CardContent>
                   <HighlightJs code={change}></HighlightJs>
                </CardContent>
            </Card>);
    }


}





export default ReplicationView;

