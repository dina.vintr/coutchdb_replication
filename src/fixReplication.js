import React, {Component} from 'react';
import './App.css';


class FixReplication extends Component {


    componentDidMount() {
        console.log('I was triggered during componentDidMount')
    }

    render() {

        console.log('I was triggered during render')

        const nano = require('nano')('http://10.200.0.166:5984');

        return (<div className="App">
                <header className="App-header">

                    <p>
                        Edit <code>src/App.js</code> and save to reload.
                    </p>
                    <button
                        onClick={function () {
                            nano.db.use('_replicator').list({include_docs: true})
                                .then((body) => {
                                    console.log(body);
                                    body.rows.forEach((item) => {
                                        console.log(item);
                                        if (item.id === "_design/_replicator" || item.id === "us12cn1" || item.id === "us12eu1") {
                                            return;
                                        }

                                        console.log(item.doc);
                                        // delete item.doc['user_ctx'];
                                        // delete item.doc['owner'];
                                        // delete item.doc['_replication_state_reason'];
                                        // delete item.doc['_replication_state'];
                                        // delete item.doc['_replication_state_time'];
                                        //
                                        //
                                        // if(   item.doc.target.headers                             )
                                        //     delete item.doc.target.headers.Authorization
                                        const selector = item.doc.selector;

                                        item.doc.selector = {"$or": [selector, {_deleted: true}]};
                                        let obj = item.doc;
                                        nano
                                            .db
                                            .use('_replicator')

                                            .insert(obj, item.id)
                                            .then((res) => console.log(res));
                                    })
                                });
                        }}
                    >
                        sdsad
                    </button>
                </header>
            </div>
        );
    }
}

export default FixReplication;
