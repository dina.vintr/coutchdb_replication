import React, {Component} from 'react';
import './App.css';
import {Rectangle, Circle, Ellipse, Line, Polyline, CornerBox, Triangle} from 'react-shapes';
import CShape from './CShape'
//import Table1 from './Table1'
// import {PouchDB, Find, Get, withDB, _withDB} from 'react-pouchdb' ;
import PouchDB from 'pouchdb';
import {ButtonToolbar} from "react-bootstrap";
// import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import PopupState, {bindTrigger, bindMenu} from 'material-ui-popup-state/index';
import Replication from "./Replication";
import DocsView from "./DocsView";
import Switch from "@material-ui/core/Switch/Switch";
import ChangesBar from "./ChangesBar";
import {DetailsPanelSetter} from "./DetailsPanel";
import HighlightJs from "./HighlightJs";
import Icon from "@material-ui/core/Icon/Icon";
import CardHeader from "@material-ui/core/CardHeader/CardHeader";

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    appBar: {
        position: 'relative',
    },
    icon: {
        marginRight: theme.spacing.unit * 2,
    },
    heroUnit: {
        backgroundColor: theme.palette.background.paper,
    },
    heroContent: {
        maxWidth: 600,
        margin: '0 auto',
        padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
    },
    heroButtons: {
        marginTop: theme.spacing.unit * 4,
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
            width: 1100,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    cardGrid: {
        padding: `${theme.spacing.unit * 8}px 0`,
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing.unit * 6,
    },
});


const ShapesList = ['Rectangle', 'Circle', 'Ellipse', 'Triangle'];
const ColorList = ['Red', 'Green', 'Purple', 'Cyan', 'Yellow'];


class DCView extends Component {


    constructor(props) {

        super(props);
        const dcs = {us1: false, eu1: false, au1: false, ru1: false, cn1: false};
        const replication = dcs;
        replication[props.db] = true;

        this.state = {color: props.color, replication: replication, docs: [], changes: [], enabled:true};


    }

    componentWillMount() {

        const db = new PouchDB(`http://10.200.0.166:5984/db1_${this.props.db}`);
        this.state.db = db;
        db.allDocs({include_docs: true}).then(res =>
            this.setState({docs: res.rows.map(row => row.doc)}));


    }

    onChange() {
        this.foo.fetchChanges();
    }

    enableChange(event) {
        this.setState({enabled:event.target.checked});
        this.feed.toggle(event.target.checked);
    }

    render() {
        let docs = this.state.docs;

        const db = this.state.db;

        return (

            <Grid item sm={6} md={4} lg={3}>
                <Card className={styles.card}>

                    <CardContent className={styles.cardContent}>

                        <footer>
                            <Typography gutterBottom variant="h5" component="h2">
                                {this.props.db}
                            </Typography>
                            <Switch checked={this.state.enabled} onChange={this.enableChange.bind(this)}/>

                        </footer>


                        <Grid container justify={"space-evenly"} direction={"column"} alignContent={'flex-start'}>

                            <ChangesBar dc={this.props.db} ref={feed => this.feed = feed} db={db}
                                        onChange={this.onChange.bind(this)} enabled={this.state.enabled}/>


                            <DocsView db={db} dc={this.props.db} ref={foo => this.foo = foo} />

                        </Grid>
                    </CardContent>


                    <CardActions>

                        <React.Fragment>
                            <Button size="small" color="primary"
                                    onClick={() => docs.map((doc) => db.remove(doc._id, doc._rev))}>
                                Clear
                            </Button>
                            <DetailsPanelSetter
                                set={<Button size="small" color="primary">
                                    <div variant="contained">
                                        Add
                                    </div>
                                </Button>}
                                details={this.renderAdd()}/>

                        </React.Fragment>
                    </CardActions>

                </Card>

            </Grid>


        );
    }

    renderAdd() {
        const db = this.state.db;

        return (
            <Card>
                <CardHeader title={`add card through ${this.props.db}`}/>

                <CardContent>

                    <Replication replication={this.state.replication}
                                 onChange={replication => this.setState({replication: replication})}/>

                </CardContent>

                <CardActions>
                    <Grid container alignContent="space-between" justify={"space-evenly"} alignItems={"flex-start"}>
                        {
                            ColorList.map((color, i) => (

                                    <Icon className={`material-icons ${color}`}
                                          onClick={() => {
                                              db.post({
                                                  color: color,
                                                  type: 'Circle',
                                                  data: 1,
                                                  origin_dc:this.props.db,
                                                  replication: this.state.replication
                                              }).catch(x => {
                                                  console.error(x)
                                              });
                                          }} key={i.toString()}>add_circle</Icon>


                                )
                            )}
                    </Grid>
                </CardActions>

            </Card>);

    }


}


export default DCView;

