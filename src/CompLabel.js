import './App.css';
import React from "react";

function CompLabel(props) {
    return (
        <React.Fragment>
            <div className="complabel-component">
                Component: <b>{props.label}</b>
            </div>
        </React.Fragment>
    );
}

function Cover(props) {
    return (
        <React.Fragment>
            <div className="cover-component">
                <CompLabel label="Cover"/>
                <h1 className="cover-head">
                    {props.head}
                </h1>
                <p className="cover-byline">
                    {props.byline}
                </p>
            </div>
        </React.Fragment>
    );
}

function Border(props) {
    return (
        <React.Fragment>
            <div className="copy-component">
                <CompLabel label={props.label}/>
                <div>{props.content}</div>
            </div>
        </React.Fragment>
    );
}


export default CompLabel;

