import './App.css';

// Utility component to highlight JSON of
// current page using:
// https://github.com/isagalaev/highlight.js

import * as React from "react";
import * as hljs from "highlight.js";

class HighlightJs extends React.Component {

    componentDidMount() {
        this.highlight();
    }

    componentDidUpdate() {
        this.highlight();
    }

    highlight = () => {
        try {
            hljs.highlightBlock(this.refs.code);
        } catch (e) {
            console.log(hljs, window.hljs);
        }
    };

    render() {
        return (
            <div className="highlightjs-component">
    <pre className={"noScroll"}>
          <code className="json" ref="code">
            {JSON.stringify(this.props.code, null, 2)}
          </code>
        </pre>
            </div>
        );
    }

}

export default HighlightJs;