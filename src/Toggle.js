import React from 'react';

class Toggle extends React.Component {
    constructor(props) {
        super(props);
         this.state = {
            isChecked: props.isChecked || false,
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange() {
        this.setState({ isChecked: !this.state.isChecked })
        this.props.onChecked(this.state.isChecked);
    }
    render () {
        return (
            <label className="switch">
                <input type="checkbox" value={this.props.isChecked} onChange={this.handleChange} />
                <div className="slider"></div>
            </label>
        );
    }
}

export default Toggle
