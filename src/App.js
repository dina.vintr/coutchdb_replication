import React, {Component} from 'react';
import './App.css';
import Album from './Album'
import Typography from "@material-ui/core/Typography/Typography";
import AppBar from "@material-ui/core/AppBar/AppBar";
import Tabs from "@material-ui/core/Tabs/Tabs";
import Tab from "@material-ui/core/Tab/Tab";
import ReplicationView from "./ReplicationView";
import {DetailsPanel} from "./DetailsPanel";

function TabContainer(props) {
    return (
        <Typography component="div" style={{ padding: 8 * 3 }}>
            {props.children}
        </Typography>
    );
}




class App extends Component {

constructor(props){
    super(props);
    this.state= {value:0};
    this.handleChange = this.handleChange.bind(this);

}

handleChange(event, newValue) {
        this.setState({value:newValue})
    }

    render() {



        const value=this.state.value;

        return (
            <div  className={"body"}>



            <link href="https://fonts.googleapis.com/icon?family=Material+Icons"  rel="stylesheet"/>

                 <header className="App-header">

                    <AppBar position="static">
                    <b   >
                        XDCR couchdb
                    </b>



                    <Tabs  value={value} onChange={this.handleChange} width={1000}  fullWidth >
                        <Tab label="poc" />
                        <Tab label="replication config" />
                    </Tabs>

                    </AppBar>
                 </header>

                <section>

                {value === 0 && <TabContainer width={1000} ><Album/>
                </TabContainer>}
                {value === 1 && <TabContainer width={1000} >
                    <ReplicationView /> </TabContainer>}

                </section>

                <aside>
                    <DetailsPanel className="details"/>

                </aside>


              </div>


        );
    }
}

export default App;
