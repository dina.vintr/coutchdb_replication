import React from 'react';
import {Circle} from "react-shapes";
import Replication from './Replication';
import Binder from 'react-binding';
import {withDB} from "react-pouchdb";
import Toggle from "./Toggle";


const dcs = {us1: false, eu1: false, au1: false, ru1: false, cn1: false};

class AddShape extends React.Component {
    constructor(props) {
        super(props);
        const db = props.db;
        this.state = {color: props.color, replication: dcs};


    }

    render() {
        return (

            <div>
                <div
                    onClick={() => {
                        this.props.db.post({
                            color: this.state.color,
                            type: this.props.type || 'Circle',
                            replication: this.state.replication
                        });
                    }}>

                    <Circle fill={{color: this.state.color}} r={10}/>
                </div>
                  <Replication  onChange={replication => this.state.replication= replication}/>
                {/*{Object.entries(this.state.replication)*/}
                    {/*.map(([dc, checked]) => (*/}
                       {/*<div> <input  key={dc}  type="checkbox" value={checked} title={dc} onChange={e => this.state.replication[dc] = e.target.checked}/>*/}
                           {/*{dc}*/}
                       {/*</div> ))}*/}

            </div>)
    }
}


function AddShapeWithDB(pdb, color) {
    const db = pdb;
    return (
        <React.Fragment>
            <AddShape db={db} color={color}/>
        </React.Fragment>
    )
}

export default withDB(({db, color}) => AddShapeWithDB(db, color));

