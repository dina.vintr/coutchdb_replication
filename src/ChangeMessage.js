 import React from "react";
import { withSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
 import Icon from "@material-ui/core/Icon/Icon";
import HighlightJs from "./HighlightJs";
 import {DetailsPanelSetter} from "./DetailsPanel";
 import Card from "@material-ui/core/Card/Card";
 import CardContent from "@material-ui/core/CardContent/CardContent";
 import IconButton from "@material-ui/core/IconButton/IconButton";
 import Grid from "@material-ui/core/Grid/Grid";
 import CardHeader from "@material-ui/core/CardHeader/CardHeader";

 const styles = {
    root: {
        flexGrow: 1,
        display: 'flex',
        margin: 16,
        justifyContent: 'center',
        alignItems: 'middle',
    },
    button: {
        margin: 8,
        color: '#fff',
        backgroundColor: '#313131',
    },
    success: {
        backgroundColor: '#43a047',
    },
    error: {
        backgroundColor: '#d32f2f',
    },
    info: {
        backgroundColor: '#2979ff',
    },
    warning: {
        backgroundColor: '#ffa000',
    },
};


class ChangeMessage extends  React.Component {


    constructor(props) {
        super(props);
        this.state = {change : props.change };
    }

    setChange(change) {
        this.setState( {change : change })  ;


        const {  enqueueSnackbar} = this.props;
        const seq = change.seq;
        const rev = change.doc._rev.split("-")[0];
        const event=  change.deleted? `deleted event rev: ${rev}` : rev == "1" ?  "created event" : `changed event rev: ${rev}` ;
        const message=`${this.props.dc}: ${event}`;
        enqueueSnackbar(message);
    }

        render() {
            const change= this.state.change;
            if(!change)
            {
                return  <div    />;
            }

            const {  enqueueSnackbar} = this.props;
            const doc= change.doc;
            const seq = change.seq;
            const seqTrimmed = change.seq.split("-")[0];
            const rev = change.doc._rev.split("-")[0];
            const data=doc.data ? doc.data : "deleted";
            const message=
                <Grid height={500} container direction={"row"} alignContent={'flex-start'}  justify={"space-evenly"} >
                    {this.renderAction(change)}

                    <b  color={doc.color} >dc: {this.props.dc} |  id: {doc._id.substring(doc._id.length-5, doc._id.length-1)} | data: {data} |  rev: {rev} | seq: {seqTrimmed}   </b>


                </Grid>



            return (


              <div key={`${seq} ${this.props.dc}`} className={`material-icons ${change.doc.color}`}> { enqueueSnackbar(message, {
                  action: <Icon className={`material-icons ${change.doc.color}`} >close</Icon>,
                  autoHideDuration: 30000  }
              )} </div>



            );
        }
    nothing(){}

    renderAction(change) {

         return (
             <DetailsPanelSetter

                 details={this.renderDetails(change)}
                 set={ <Icon className={`material-icons ${change.doc.color}`} >open_in_new</Icon> }
                 />


  );

    }


    renderDetails(change) {
        const seqTrimmed = change.seq.split("-")[0];

        return (
            <Card >
                <CardHeader title={`change seq ${seqTrimmed} details`} />
                 <CardContent  >
                    <HighlightJs code={change}></HighlightJs>
                </CardContent>

            </Card>);

    }

}

ChangeMessage.propTypes = {
    classes: PropTypes.object.isRequired,
    enqueueSnackbar: PropTypes.func.isRequired,
};

export default withStyles(styles)(
    withSnackbar(ChangeMessage),);





