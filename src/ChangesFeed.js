import React, {Component} from 'react';
import {Circle} from "react-shapes";
import Typography from "@material-ui/core/Typography/Typography";
import CardContent from "@material-ui/core/CardContent/CardContent";
import PouchDB from 'pouchdb';
import Button from '@material-ui/core/Button';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import Tooltip from '@material-ui/core/Tooltip';
import Menu from "@material-ui/core/Menu/Menu";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import PopupState, {bindTrigger, bindMenu, bindToggle, bindPopper} from 'material-ui-popup-state/index';
import Popper from '@material-ui/core/Popper';
import Fade from '@material-ui/core/Fade';
import Paper from '@material-ui/core/Paper';
import Switch from '@material-ui/core/Switch';
import HighlightJs from './HighlightJs';

// const PouchDB: typeof Pouch = require('pouchdb');

// import {PouchDB}  from 'pouchdb';


class ChangesFeed extends Component {
    constructor(props) {
        super(props);
        this.state = {changes: [], deleted: [], onChange: props.onChange, enabled: false};
    }

    componentWillMount() {
        this.enabledChanged();
    }

    enabledChanged() {
        if (this.state.enabled) {
            this.fetchChanges( );
        }
        else if (this.state.subscriber) {
            this.state.subscriber.cancel();
        }
    }

    toggle(toggle){
        this.setState({enabled: toggle});
    }

    componentDidUpdate(prevProps, prevState, ss) {
        if (this.state.enabled != prevState.enabled)
            this.enabledChanged();

    }

    fetchChanges( ) {
        const db = this.props.db
        const subscriber = db.changes({
            since: 'now',
            include_docs: true,
            conflicts: true,
            live: true,
            heartbeat: 50000,
            timeout: 50000
        })
            .on('change', change => {

                if (this.state.onChange)
                    this.state.onChange(change)
                const changes = this.state.changes;
                changes.push(change);
                this.setState({changes: changes});

            })
            .on('error', err => console.log(err));

        this.state.subscriber = subscriber;
    }


    componentWillUnmount() {
        if( this.state.subscriber)
        this.state.subscriber.cancel();
    }




    render() {
        let changes = this.state.changes;
        return (
            <div>

                 <Typography>
                    Changes feed:
                </Typography>
                  {changes.map(ch => this.renderChange(ch))}
            </div>
                );}





    renderChange(change) {
        return (<PopupState key={change.seq} variant="popper" popupId="demo-popup-popper">
            {popupState => (
                <div>
                    <div variant="contained" {...bindToggle(popupState)}>
                         <Circle fill={{color: change.doc.color}} r={10} />
                    </div>
                    <Popper
                        variant="contained"
                        wight={100}
                        zIndex={1}
                        placement="bottom-end"
                        disablePortal={false}
                        modifiers={{
                            flip: {
                                enabled: true,
                            },
                            preventOverflow: {
                                enabled: true,
                                boundariesElement: 'scrollParent',
                            },
                        }}
                        {...bindPopper(popupState)} transition>
                        {({TransitionProps}) => (
                            <Fade {...TransitionProps} timeout={350}>
                                <Paper>
                                    <React.Fragment>
                                        <b>change view:</b>
                                        {this.renderDetails(change)}
                                        <Button color="primary" {...bindToggle(popupState)}  >Close</Button>

                                    </React.Fragment>

                                </Paper>
                            </Fade>
                        )}
                    </Popper>
                </div>
            )}
        </PopupState>);

    }


     renderDetails(change) {return (<HighlightJs code={change}></HighlightJs>);}




}

export default ChangesFeed





// export default withDB(({db  }) => ChangesFeed(db ));


