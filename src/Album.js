import React from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import DCView from './DCView'
import ChangesFeed from './ChangesFeed'
import {withStyles} from '@material-ui/core/styles';
import {SnackbarProvider, withSnackbar} from 'notistack';
import CounterContainer, {DetailsPanel, DetailsPanelSetter} from "./DetailsPanel";


const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    icon: {
        marginRight: theme.spacing.unit * 2,
    },
    heroUnit: {
        backgroundColor: theme.palette.background.paper,
    },
    heroContent: {
        maxWidth: 600,
        margin: '0 auto',
        padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
    },
    heroButtons: {
        marginTop: theme.spacing.unit * 4,
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
            width: 1100,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    cardGrid: {
        padding: `${theme.spacing.unit * 8}px 0`,
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing.unit * 6,
    },
});

const cards = ['us1', 'eu1', 'au1', 'ru1', 'cn1'];

function Album(props) {
    const {classes} = props;
    return (
        <React.Fragment>
                 <SnackbarProvider maxSnack={10} onClickAction={() => console.debug("click")} transitionDuration={{ exit: 100, enter: 100 }}>

                    <CssBaseline/>

                    <div className={classNames(classes.layout, classes.cardGrid)}>
                        <Grid container spacing={40}>
                            {cards.map(card => (<DCView key={card} db={card}></DCView>))}
                        </Grid>
                    </div>
                </SnackbarProvider>


        </React.Fragment>
    );
}

Album.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Album);