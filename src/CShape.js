import React, {Component} from 'react';
import {Rectangle, Circle, Ellipse, Line, Polyline, CornerBox, Triangle} from 'react-shapes';

import ReactDOM from 'react-dom';
import App from './App';
import DCView from "./DCView";

class  CShape extends  Component {

    constructor(props) {
        super(props);
    }






  render( ) {
      switch (this.props.type) {
          case "Rectangle":
              return (
                  <Rectangle fill={this.props.fill} stroke={this.props.stroke} strokeWidth={this.props.strokeWidth} />
              );

          case  "Circle":
              return (<Circle fill={this.props.fill} stroke={this.props.stroke}
                              strokeWidth={this.props.strokeWidth}  ></Circle>);

          case  "Ellipse":
              return (<Ellipse fill={this.props.fill} stroke={this.props.stroke}
                               strokeWidth={this.props.strokeWidth} width={40} height={40}></Ellipse>);

          case  "Polyline":
              return (<Polyline fill={this.props.fill} stroke={this.props.stroke}
                                strokeWidth={this.props.strokeWidth} ></Polyline>);

          case  "Triangle":
              return (<Triangle fill={this.props.fill} stroke={this.props.stroke}
                                strokeWidth={this.props.strokeWidth} width='100%' height='100%'></Triangle>);

          case  "CornerBox":
              return (<CornerBox fill={this.props.fill} stroke={this.props.stroke}
                                 strokeWidth={this.props.strokeWidth} width='100%' height='100%'></CornerBox>);

          default:
              return (
                  <Line fill={this.props.fill} stroke={this.props.stroke} strokeWidth={this.props.strokeWidth}></Line>);

      }

  }
}

export default CShape;

