import React, {Component} from 'react';
import PouchDB from 'pouchdb';
import HighlightJs from './HighlightJs';
import Grid from "@material-ui/core/Grid/Grid";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Chip from '@material-ui/core/Chip';
import Icon from '@material-ui/core/Icon';
import {DetailsPanelSetter} from "./DetailsPanel";
import CardHeader from "@material-ui/core/CardHeader/CardHeader";


function createConflict(doc, db, dc) {


    const dc2 = first(Object.entries((doc.replication)), (([dcTarget, checked]) => (dcTarget !== dc && checked)));

    const db2 = new PouchDB(`http://10.200.0.166:5984/db1_${dc2 && dc2[0]}`);

    doc.data = doc.data ? doc.data + 1 : 1;
    doc.origin_dc = dc2;
    db2.post(doc).catch(console.error);

    doc.data = doc.data ? doc.data + 1 : 1;
    doc.origin_dc = dc;
    db.post(doc).catch(console.error);
}

function first(array, predicate) {
    for (var i = 0; i < array.length; ++i) {
        if (predicate(array[i])) return array[i];
    }
    alert("no match target");
}

class DocsView extends Component {
    constructor(props) {
        super(props);

        this.state = {docs: [], db: props.db};
    }

    componentWillMount() {
        this.fetchChanges();
    }


    fetchChanges() {
        this.state.db.allDocs({include_docs: true, conflicts: true}).then(res =>
            this.setState({docs: res.rows.map(row => row.doc)}));
    }


    render() {

        let docs = this.state.docs;
        let db = this.state.db;
        return (
            <div>

                <Grid container direction="row"
                      justify="space-evenly"
                      alignItems="center"
                      spacing={24}>
                    {docs.map(ch => this.renderDoc(ch))}
                </Grid>
            </div>
        );
    }


    renderDoc(doc) {
        let db = this.state.db;

        const rev = doc._rev.split("-")[0];

        return (


            <DetailsPanelSetter

                details={this.renderDetails(doc)}
                set={<Chip
                    className={`chip default`}
                    variant={"outlined"}
                    width={900}
                    onDelete={() => {
                        doc._deleted= true;
                        db.post(doc).catch(console.error);
                    }}
                    label={<div height={500}>
                        <b>
                            <pre>data: {doc.data ? doc.data : 0}</pre>
                            <pre>rev: {rev} </pre>
                            <pre>id: {doc._id.substring(doc._id.length - 5, doc._id.length - 1)} </pre>

                        </b></div>}
                    deleteIcon={<Icon className={`material-icons ${doc.color}`}
                    >delete_outlined</Icon>}
                    icon={
                        <div>
                            <Icon className={`material-icons ${doc.color}`}
                                  onClick={() => {
                                      doc.data = doc.data ? doc.data + 1 : 1;
                                      db.post(doc).catch(console.error);
                                  }}>edit</Icon>
                            <pre/>
                            <Icon className={`material-icons ${doc.color}`}
                                  onClick={() => {
                                      createConflict(doc, db, this.props.dc);
                                  }}>transform</Icon>
                        </div>}
                />}/>);


    }


    renderDetails(doc) {
        return (
            <Card >
                <CardHeader title={`doc ${doc._id.substring(doc._id.length - 5, doc._id.length - 1)} details:`}/>
                 <CardContent  >
                    <HighlightJs code={doc}></HighlightJs>
                </CardContent>

            </Card>);

    }


}

export default DocsView




